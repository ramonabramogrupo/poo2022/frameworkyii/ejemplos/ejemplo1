<?php

use yii\helpers\Html;

echo Html::img(
        "@web/imgs/f1.png",
        [
            "alt" => "imagen 1",
            "class" => "col-4 img-thumbnail" 
        ]
        );
echo Html::img("@web/imgs/f2.png",
        [
            "alt" => "imagen 2",
            "class" => "col-4 img-thumbnail" 
        ]);
echo Html::img("@web/imgs/f3.png",
        [
            "alt" => "imagen 3",
            "class" => "col-4 img-thumbnail" 
        ]);
echo Html::img("@web/imgs/f4.png",
        [
            "alt" => "imagen 4",
            "class" => "col-4 img-thumbnail" 
        ]);

