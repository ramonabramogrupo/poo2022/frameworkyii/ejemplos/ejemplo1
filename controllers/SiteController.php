<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionMensaje(){
        return $this->render('mostrarMensaje');
    }
    
    public function actionImagen(){
        return $this->render("imagen");
    }
    
    public function actionSemana(){
        return $this->render("semana");
    }
    
    public function actionMeses(){
        return $this->render("meses");
    }
    
    public function actionImagenes(){
        return $this->render("imagenes");
    }
    
    public function actionFoto($id=1){
        $imagenes=[
            "f1.png",
            "f2.png",
            "f3.png",
            "f4.png",
        ];
        
        
        return $this->render("foto",[
            "foto" => $imagenes[$id]
        ]);
        
    }


}
